<?php

/**  
 * @file  
 * Contains Drupal\php_runtime_memory\Form\PHPRuntimeMemoryForm.  
 */  

namespace Drupal\php_runtime_memory\Form;  

use Drupal\Core\Form\ConfigFormBase;  
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\FileSecurity\FileSecurity; 

/**
 * PHP Runtime Memory admin configuraiton form.
 */

class PHPRuntimeMemoryForm extends ConfigFormBase {
  /**  
   * {@inheritdoc}  
   */  
  protected function getEditableConfigNames() {  
    return [  
      'php_runtime_memory.adminsettings',  
    ];  
  } 

  /**  
   * {@inheritdoc}  
   */  
  public function getFormId() {  
    return 'php_runtime_memory_admin_form';  
  }

  /**  
   * {@inheritdoc}  
   */  
  public function buildForm(array $form, FormStateInterface $form_state) {  
    $config = $this->config('php_runtime_memory.adminsettings');
    
    // Set file upload fields
    $form['file_upload_limit_group'] = [
        '#type' => 'details',
        '#title' => $this->t('file upload limit'),
        '#collapsible' => TRUE,
        '#weight' => 0,
        '#open' => TRUE,
    ];
    
    $form['file_upload_limit_group']['custom_upload_limit_turn_option'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Enable file upload limit'),
        '#default_value' => $config->get('custom_upload_limit_turn_option'),
        '#description' => t('Enable/Disable custom file upload limit. If you uncheck this option then it will not affect anything even if its active on modules page.')
    ];

    $form['file_upload_limit_group']['custom_upload_limit'] = [
        '#type' => 'textfield',
        '#title' => $this->t('File upload limit'),
        '#default_value' => $config->get('custom_upload_limit'),
        '#description' => $this->t('Enter the size in MB. Current file upload limit is ' . '<b>' . ini_get('upload_max_filesize') . '</b>' . '.'),
        '#maxlength' => 4,
        '#size' => 5,
        '#required' => TRUE,
    ];

    // Set memory limit fields
    $form['memory_limit_group'] = [
        '#type' => 'details',
        '#title' => $this->t('Memory limit'),
        '#collapsible' => TRUE,
        '#weight' => 0,
        '#open' => TRUE,
    ];
  
    $form['memory_limit_group']['custom_memory_limit_turn_option'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Enable memory limit'),
        '#default_value' => $config->get('custom_memory_limit_turn_option'),
        '#description' => $this->t('Enable/Disable custom memory limit. If you uncheck this option then it will not affect anything even if its active on modules page.'),
    ];
  
    $form['memory_limit_group']['custom_memory_limit'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Memory limit'),
        '#default_value' => $config->get('custom_memory_limit'),
        '#description' => $this->t('Enter the size in MB. Current memory limit is ' . '<b>' . ini_get('memory_limit') . '</b>' . '.'),      
        '#maxlength' => 4,
        '#size' => 5,
        '#required' => TRUE,
    ];

    // Set maximum execution limit fields
    $form['memory_execution_group'] = [
        '#type' => 'details',
        '#title' => $this->t('Maximum execution time limit'),
        '#collapsible' => TRUE,
        '#weight' => 0,
        '#open' => TRUE,
    ];
  
    $form['memory_execution_group']['custom_max_execution_time_limit_turn_option'] = [
        '#type' => 'checkbox',
        '#title' => t('Enable maximum execution time limit'),
        '#default_value' => $config->get('custom_max_execution_time_limit_turn_option'),
        '#description' => t('Enable/Disable custom maximum execution time limit. If you uncheck this option then it will not affect anything even if its active on modules page.')
    ];
  
    $form['memory_execution_group']['custom_max_execution_time_limit'] = [
        '#type' => 'textfield',
        '#title' => t('Maximum execution time limit'),
        '#default_value' => $config->get('custom_max_execution_time_limit'),
        '#description' => t('Enter limit in Seconds. Current execution time limit is ' . '<b>' . ini_get('max_execution_time') . ' seconds</b>' . '.'),
        '#maxlength' => 4,
        '#size' => 5,
        '#required' => TRUE,
    ];

  // Set maximum input variable fields
    $form['max_input_vars_group'] = [
      '#type' => 'details',
      '#title' => $this->t('Maximum input variables field'),
      '#collapsible' => TRUE,
      '#weight' => 0,
      '#open' => TRUE,
    ];
  
  
    $form['max_input_vars_group']['custom_max_input_vars_limit_turn_option'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable maximum input variable limit'),
      '#default_value' => $config->get('custom_max_input_vars_limit_turn_option'),
      '#description' => $this->t('Enable/Disable custom maximum input variable limit. If you uncheck this option then it will not affect anything even if its active on modules page.')
    ];
  
    $form['max_input_vars_group']['custom_max_input_vars_limit'] = [
      '#type' => 'textfield',
      '#title' => t('Maximum input variable limit'),
      '#default_value' => $config->get('custom_max_input_vars_limit'),
      '#description' => $this->t('Enter limit. Current maximum input variable limit is ' . '<b>' . ini_get('max_input_vars') . '</b>' . '.'), 
      '#maxlength' => 4,
      '#size' => 5,
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);  
  }
  
  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    
    if (!is_numeric($form_state->getValue('custom_upload_limit')) && $form_state->getValue('custom_upload_limit') <= 0 ) {
      $form_state->setErrorByName('custom_upload_limit', $this->t('Please enter a valid upload limit'));
    }

    if (!is_numeric($form_state->getValue('custom_memory_limit')) && $form_state->getValue('custom_memory_limit') <= 0 ) {
      $form_state->setErrorByName('custom_memory_limit', $this->t('Please enter a valid memory limit'));
    }

    if (!is_numeric($form_state->getValue('custom_max_execution_time_limit')) && $form_state->getValue('custom_max_execution_time_limit') <= 0 ) {
      $form_state->setErrorByName('custom_max_execution_time_limit', $this->t('Please enter a valid Memory execution time'));
    }

    if (!is_numeric($form_state->getValue('custom_max_input_vars_limit')) && $form_state->getValue('custom_max_input_vars_limit') <= 0 ) {
      $form_state->setErrorByName('custom_max_input_vars_limit', $this->t('Please enter a valid Memory input variable limit'));
    }

    $filename = '.htaccess';
    if (!is_writeable($filename)) {
      $form_state->setErrorByName($filename, $this->t('.htaccess is not writable'));
    }
    
    
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $config = $this->config('php_runtime_memory.adminsettings');
    $config->set('custom_upload_limit_turn_option', $form_state->getValue('custom_upload_limit_turn_option'))->save();
    $config->set('custom_memory_limit_turn_option', $form_state->getValue('custom_memory_limit_turn_option'))->save();
    $config->set('custom_max_execution_time_limit_turn_option', $form_state->getValue('custom_max_execution_time_limit_turn_option'))->save();
    $config->set('custom_max_input_vars_limit_turn_option', $form_state->getValue('custom_max_input_vars_limit_turn_option'))->save();
    $config->set('custom_upload_limit', $form_state->getValue('custom_upload_limit'))->save();
    $config->set('custom_memory_limit', $form_state->getValue('custom_memory_limit'))->save();
    $config->set('custom_max_execution_time_limit', $form_state->getValue('custom_max_execution_time_limit'))->save();
    $config->set('custom_max_input_vars_limit', $form_state->getValue('custom_max_input_vars_limit'))->save();
    $filename = '.htaccess';
    $file_as_array = file($filename);
    $$filter_file_list = [];

    if ($form_state->getValue('custom_upload_limit_turn_option') || $form_state->getValue('custom_memory_limit_turn_option') || $form_state->getValue('custom_max_execution_time_limit_turn_option') || $form_state->getValue('custom_max_input_vars_limit')) {
      $file_as_array = \Drupal::service('php_runtime_memory.delete_file')->deleteHtaccess($file_as_array, $filter_file_list = []);

      if ($form_state->getValue('custom_upload_limit_turn_option')) {
        $new_limit = $form_state->getValue('custom_upload_limit');
  
        $file_as_array[] = "\n" . 'php_value post_max_size ' . $new_limit . 'M' . "\n";
        $file_as_array[] = 'php_value upload_max_filesize ' . $new_limit . 'M';
        $filter_file_list = ['post_max_size', 'upload_max_filesize'];
      }
      if ($form_state->getValue('custom_memory_limit_turn_option')) {
        $new_limit = $form_state->getValue('custom_memory_limit');	
        
        $file_as_array[] = "\n" . 'php_value memory_limit ' . $new_limit . 'M' . "\n";
        $filter_file_list[] = 'memory_limit'; 	  
      }
      if ($form_state->getValue('custom_max_execution_time_limit_turn_option')) { 
        $new_limit = $form_state->getValue('custom_max_execution_time_limit');	
        
        $file_as_array[] = "\n" . 'php_value max_execution_time ' . $new_limit . "\n"; 	
        $filter_file_list[] = 'max_execution_time';
      }    
      if ($form_state->getValue('custom_max_input_vars_limit_turn_option')) { 
        $new_limit = $form_state->getValue('custom_max_input_vars_limit');  
        
        $file_as_array[] = "\n" . 'php_value max_input_vars ' . $new_limit . "\n";  
        $filter_file_list[] = 'max_input_vars';
      }

      if (empty($form_state->getValue('custom_upload_limit_turn_option')) || empty($form_state->getValue('custom_memory_limit_turn_option')) || empty($form_state->getValue('custom_max_execution_time_limit_turn_option')) || empty($form_state->getValue('custom_max_input_vars_limit_turn_option'))) { 
        $file_as_array = \Drupal::service('php_runtime_memory.delete_file')->deleteHtaccess($file_as_array, isset($filter_file_list) ? $filter_file_list : []);
      }

      $new_file_contents = implode("", $file_as_array);
      $fp = @fopen($filename, 'w');
      fwrite($fp, $new_file_contents);
      fclose($fp);
    }

  }
  
}
