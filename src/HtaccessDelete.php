<?php

/**
 * @file providing the service that delete .htaccess data.
 *
 */

 namespace  Drupal\php_runtime_memory;

 class HtaccessDelete {
    

    public function deleteHtaccess($file_as_array, $filter_file_list = []) {
    
      $full_filter_file_list = array('post_max_size', 'upload_max_filesize', 'memory_limit', 'max_execution_time', 'max_input_vars');                            
      $final_filter_list = array_diff($full_filter_file_list, $filter_file_list);                                  
      $final_filter_list = implode('|', $final_filter_list);
                                   
      foreach ($file_as_array as $file_array_key => $file_array_val) {                                     
        if (!empty($final_filter_list) && preg_match('/' . $final_filter_list . '/', $file_array_val)) {   
            unset($file_as_array[$file_array_key]);
        } 
      }
      
      return $file_as_array;
      
    }

 }

